# Тестовое задание
## Деплой приложений ArgoCD, Nginx, Loki-stack в kind
### Требования:
- Helm
- kubectl
- kind (localhost)
### Репзитории:
- [ArgoCD Helm](https://gitlab.com/ZergiShark/argocd-helm.git)
- [Loki stack](https://gitlab.com/ZergiShark/loki-stack.git)
- [Nginx](https://gitlab.com/ZergiShark/nginx.git)
### Для запуска:
1. Создать кластер kind с помощью конфигурации:
```
kind create cluster --config kind.yaml
```
2. Склонировать репозиторий ArgoCD и запустить:
```
git clone https://gitlab.com/ZergiShark/argocd-helm.git
cd argocd-helm
helm install argocd ./argocd --namespace=argocd --create-namespace
```
3. Получить пароль от ArgoCD и с помощью port-forward попасть в web-интерфейс:
``` 
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
kubectl port-forward svc/argocd-server -n argocd 8080:443
```
4. Развернуть приложения Loki-stack и Nginx:
```
kubectl apply -f https://gitlab.com/ZergiShark/argocd/-/raw/main/apps/Loki-stack/application.yaml
kubectl apply -f https://gitlab.com/ZergiShark/argocd/-/raw/main/apps/Nginx/application.yaml
```
5. Узнать пароль от Grafana и зайти через port-forward:
```
kubectl get secret --namespace loki-stack loki-stack-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
kubectl port-forward --namespace loki-stack service/loki-grafana 3000:80
```
6. Добавить приложение ArgoCD через веб-интерфейс

### Скриншоты
![argocd](/screenshots/argocd.png)
![nginx](/screenshots/nginx.png)
![grafana](/screenshots/grafana.png)
